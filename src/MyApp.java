import java.util.Scanner;

public class MyApp {
    static void printWelcome(){
        System.out.println("Welcome to my app!!!");
    }
    static void printMenu(){
        System.out.println("--Menu--");
        System.out.println("1. print Helllo World N times");
        System.out.println("2. Add 2 number");
        System.err.println("3. Exit");
    }

    static int inputChoice(){
        int choice;
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input your choice(1-3): ");
            choice = sc.nextInt();
            if(choice >= 1 && choice <= 3){
                return choice;
            } 
            System.out.print("Error: Please input between 1-3");
        }
    }
    public static void main(String[] args) {
        int choice = 0;
        while(true){
            printWelcome();
            printMenu();
            choice = inputChoice();
            switch(choice){
                case 1:
                    printHelloWorld();  
                    break;
                case 2:
                    addTwoNumber();
                    break;
                case 3:
                    exitProgram();
                    break;   
            }
        }
    }
    static int add(int frist, int second){
        int result = frist + second;
        return result;
    }
    static void addTwoNumber(){
        int frist, secound;
        int result;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input frist number: ");
        frist = sc.nextInt();
        System.out.print("Please input second number: ");
        secound = sc.nextInt();
        result = add(frist, secound);
        System.out.println("Result = "+ result);
    }
    static int inputTime(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input time: ");
        int time = sc.nextInt();
        return time;
    }
    static void printHelloWorld(){
        int time = inputTime();
        for(int i = 0; i<time; i++){
            System.out.println("Hello World!!!");
        }
    }
    static void exitProgram() {
        System.exit(0);
        System.out.println("Bye!!!");
    }
}
